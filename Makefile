#!/usr/bin/env make
packages = $(patsubst grumpy/%,public/%,$(wildcard grumpy/*))

clean:
	rm -rf public/

init:
	helm init --client-only

public:
	mkdir -p ./public
	echo 'User-Agent: *\nDisallow: /\' > ./public/robots.txt

public/%:
	helm package grumpy/$(*F) --destination ./public

public/index.yaml: public $(packages)
	helm repo index --url https://${CI_PROJECT_NAMESPACE}.gitlab.io/${CI_PROJECT_NAME} .
	mv index.yaml ./public/index.yaml

build: init public/index.yaml
	@echo "index built"
